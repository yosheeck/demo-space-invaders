#include "lib/leetlib.h"
#include <math.h>
#include <cstdio>
#include <cstring>

//
// TODO: Move to external file.
//

class KemuClient
{
private:

  const char *_host;
  int _port;
  SOCKET _socket;

public:

  KemuClient(const char *host, int port);

  int connect();
  int disconnect();
  int sendState(int x, int y, int score);
};

KemuClient::KemuClient(const char *host, int port)
{
  _host = host;
  _port = port;
  _socket = -1;
}

int KemuClient::connect()
{
  int exitCode = -1;

  struct sockaddr_in sa = { 0 };

  //
  // Initialize WinSock 2.2 on windows.
  //

  WSADATA wsaData = { 0 };

  WORD version = MAKEWORD(2, 2);

  WSAStartup(version, &wsaData);

  //
  // Create empty TCP socket.
  //

  _socket = socket(AF_INET, SOCK_STREAM, 0);

  //
  // Connect to server.
  // TODO: Resolve host to ip.
  //

  sa.sin_family = AF_INET;
  sa.sin_addr.s_addr = inet_addr(_host);
  sa.sin_port = htons(_port);

  if (::connect(_socket, (struct sockaddr *) &sa, sizeof(sa)) != 0)
  {
	  OutputDebugString("ERROR: Can't connect to server\n");
	  //OutputDebugString("Error code is: [%d].\n", WSAGetLastError());
  }
  else
  {
	  OutputDebugString("Connected OK !\n");
  }
  exitCode = 0;

fail:

  return exitCode;
}

int KemuClient::disconnect()
{
  //
  // TODO
  //

  return -1;
}

int KemuClient::sendState(int x, int y, int score)
{
  int exitCode = 0;
  char msg[128] = { 0 };
  int msgLen = _snprintf(msg, sizeof(msg) - 1, "x,y,score\n%d,%d,%d\n", x, y, score);
  int bytesSent = send(_socket, msg, msgLen, 0);

  OutputDebugString("Send\n");

  if (bytesSent != msgLen)
  {
    printf("ERROR: Can't send state to server.\nError code is [%d].\n", WSAGetLastError());

    exitCode = -1;
  }

  return exitCode;
}

struct bullet
{
	float BX,BY,BA;
	bullet() {BX=BY=BA=0;}
};

//
// Global variables.
//

int x[50];
int y[50];
int time=0;
int UX=400, UY=550; // Current user's position.
int KemuClientTheadEnabled = 1;

HANDLE KemuClientThread = NULL;

bullet bullets[10];

//
// Background thread to send current state to KEMU server each 50ms.
//

DWORD WINAPI KemuClientEntry(void *unused)
{
  int lastUX = -1;
  int lastUY = -1;

#if 1
  KemuClient *kemuClient = new KemuClient("52.16.174.165", 8886);
#else
  KemuClient *kemuClient = new KemuClient("127.0.0.1", 8886);
#endif
  kemuClient->connect();

  while (KemuClientTheadEnabled)
  {
    if (UX != lastUX || UY != lastUY)
    {
      kemuClient -> sendState(UX, UY, 0);

      lastUX = UX;
      lastUY = UY;
    }

    Sleep(50);
  }

  return 0;
}

//
// Game core.
//

void Game()
{
  //
  // Init background thead sending game state to KEMU erver.
  //

  KemuClientThread = CreateThread(NULL, 0, (LPTHREAD_START_ROUTINE) KemuClientEntry, NULL, 0, NULL);


	void *Text[]=
	{
		LoadSprite("gfx/slet.png"),
		LoadSprite("gfx/plet.png"),
		LoadSprite("gfx/alet.png"),
		LoadSprite("gfx/clet.png"),
		LoadSprite("gfx/elet.png"),
		0,
		LoadSprite("gfx/ilet.png"),
		LoadSprite("gfx/nlet.png"),
		LoadSprite("gfx/vlet.png"),
		LoadSprite("gfx/alet.png"),
		LoadSprite("gfx/dlet.png"),
		LoadSprite("gfx/elet.png"),
		LoadSprite("gfx/rlet.png"),
		LoadSprite("gfx/slet.png")
	};

	// SETUP
	void *Enemy = LoadSprite("gfx/Little Invader.png");
	void *U = LoadSprite("gfx/Big Invader.png");
	void *bull = LoadSprite("gfx/bullet.png");
	for(int n=0;n<50;++n)
	{
		x[n]=(n%10)*60+120;
		y[n]=(n/10)*60+70;
	}
end:
	++time;
	if(WantQuit()) return;
	if(IsKeyDown(VK_ESCAPE)) return;
	for(int n=0;n<50;++n)
	{
		int xo=0,yo=0;
		int n1=time+n*n+n*n*n;
		int n2=time+n+n*n+n*n*n*3;
		if(((n1>>6)&0x7)==0x7)xo+=(1-cos((n1&0x7f)/64.0f*2.f*3.141592))*(20+((n*n)%9));
		if(((n1>>6)&0x7)==0x7)yo+=(sin((n1&0x7f)/64.0f*2.f*3.141592))*(20+((n*n)%9));
		if(((n2>>8)&0xf)==0xf)yo+=(1-cos((n2&0xff)/256.0f*2.f*3.141592))*(150+((n*n)%9));
		DrawSprite(Enemy, x[n]+xo, y[n]+yo, (10+((n)%17)), (10+((n)%17)), 0, 0xffffffff);
	}

	DrawSprite(U, UX+=IsKeyDown(VK_LEFT)?-7:IsKeyDown(VK_RIGHT)?7:0, UY, 50, 50, 3.141592+sin(time*0.1)*0.1, 0xffffffff);

	// FIRE
	static int b=0;
	static int count=0;
	if(count) --count;
	if(!IsKeyDown(VK_SPACE)) count=0;
	if(IsKeyDown(VK_SPACE) && count==0) {bullets[b].BX=UX; bullets[b].BY=UY; b=(b+1)%10; count=15;}

	for(int n=0;n<10;++n)
	{
		DrawSprite(bull, bullets[n].BX, bullets[n].BY-=4, 10, 10, bullets[n].BA+=0.1f, 0xffffffff);
	}

	for(int n=0;n<strlen("space invaders");++n) if(n!=5)DrawSprite(Text[n], n*40+150,30,20,20,sin(time*0.1)*n*0.01);

	Flip();

	goto end;
}

void OldGame()
{
	void *sprite = LoadSprite("sprite.png");
	float size=10;
	float angle=0;
	while (!WantQuit() && !IsKeyDown(VK_ESCAPE))
	{
		angle+=0.01f;
		float mx,my;
		GetMousePos(mx,my);
		DWORD col=0xffffffff; // white
		if (IsKeyDown(VK_LBUTTON)) col=0xffff0000; // solid red
		if (IsKeyDown(VK_RBUTTON)) col=0x8000ff00; // 50% transparent green
		if (IsKeyDown(VK_UP)) size++;
		if (IsKeyDown(VK_DOWN)) size--;

		DrawSprite(sprite,400,300,100,100, angle);
		DrawSprite(sprite, mx,my, size,size, 0, col);
		Flip();
	}
}
